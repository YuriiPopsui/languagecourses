import { timeSlots, lessons} from "./constants";

let booking = {};
booking.name = JSON.parse(localStorage.getItem('login')).name;

const slots = document.querySelectorAll('[name=time]');
const types = document.querySelectorAll('[name=type]');
const bookBtn = document.querySelector('#book_lesson');

slots.forEach(elem => {
  elem.addEventListener('click', () => {
    if(elem.id === 'time_01' || elem.id === 'time_02' || elem.id === 'time_03') {
      booking.tomorrow = false;
    } else {
      booking.tomorrow = true;
    }
    booking.time = timeSlots[`${elem.id}`];
  });
});
types.forEach(elem => {
  elem.addEventListener('click', () => {
    booking.title = lessons[`${elem.id}`].title;
    booking.duration = lessons[`${elem.id}`].duration;
  });
});

bookBtn.addEventListener('click', () => {
  if(validateBooking()) {
    let entries = JSON.parse(localStorage.getItem('lessons')) || [];
  entries.push(booking);
  localStorage.setItem('lessons', JSON.stringify(entries)); 
  } else {
    alert("Выберите все необходимые условия.");
  }

});
function validateBooking() {
  let validate = true;
  let valueArr = Object.values(booking);
  if(valueArr.length !== 5) {
    validate = false;
  }
  return validate;
}
console.log('lessons form');
