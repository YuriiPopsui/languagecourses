import { saveToStorage } from './login';

/// Task #1 Transitions between forms  ///
const loginForm = document.querySelector('#loginBlock');
const roleForm = document.querySelector('#step1Block');
const regForm = document.querySelector('#regBlock');
const regBtn = document.querySelector('#regBtn');
const continueBtn = document.querySelector('#toStep2Btn');
const from2to1Btn = document.querySelector('#toLoginSvg');
const from3to2Btn = document.querySelector('#from3to2Svg');

regBtn.addEventListener('click', () => {
  loginForm.classList.add('hidden');
  roleForm.classList.add('show');
});
from2to1Btn.addEventListener('click', () => {
  loginForm.classList.remove('hidden');
  roleForm.classList.remove('show');
});

continueBtn.addEventListener('click', () => {
  roleForm.classList.remove('show');
  regForm.classList.add('show');
});
from3to2Btn.addEventListener('click', () => {
  roleForm.classList.add('show');
  regForm.classList.remove('show');
});

/// Task #2 Registration-forms logic  /// 

let users = {}; 
const teacherLabel = document.querySelector('[for=user_teacher]');
const studentLabel = document.querySelector('[for=user_student]');
const inputs = document.querySelectorAll('.input-box');
// const labels = document.querySelectorAll('.user-type');
const createBtn = document.querySelector('#createAccount');

teacherLabel.addEventListener('click', () => {
  users.type = 'teacher';
});
studentLabel.addEventListener('click', () => {
  users.type = 'student';
 
});

// Здесь проблема. Почему-то не могу получить доступ к атрибуту for в label.
// Хотя таким же способом получил доступ к атрибуту name в input.

// labels.forEach(elem => {
//   elem.addEventListener('click', () => {
//     if (elem.for === 'user_student') {
//       users.type = 'student';
//       console.log(users);
//     } else {
//       users.type = 'teacher';
//       console.log(users);
//     }
//   });
// });

let pass = [];
inputs.forEach((elem) => {
  elem.addEventListener('change', () => {
    if(elem.name !== 'password') {
      users[elem.name] = elem.value;
      //console.log(users);
    } else {
        pass.push(elem.value);
      if(pass[1]) {
        if (pass[0] === pass[1]) {
        users.password = pass[0];
        } 
      }
      //console.log(users);
    }
  });
});
createBtn.addEventListener('click', () => {
  if(users.type === 'teacher') {
    localStorage.setItem('teacher', JSON.stringify(users));
  }
  if(users.type === 'student') {
    let entries = JSON.parse(localStorage.getItem('students')) || [];
    entries.push(users);
    localStorage.setItem('students', JSON.stringify(entries)); 
  }
  function validate() {
    let validate = true;
    if (pass[0] !== pass[1]) {
      document.querySelector('#password').classList.add('error');
      document.querySelector('#password_next').classList.add('error');
      validate = false;
    }
    let nameValidate = users.name.split(' ');
    if (nameValidate.length !== 2 || nameValidate[0].length < 3 || nameValidate[1].length < 3) {
      document.querySelector('#name').classList.add('error');
      validate = false;
    }
    return validate;
  }
  if (validate()) {
    if(users.type === 'student') {
    window.location.href = './student.html';
    } else {
    window.location.href = './teacher.html';
    }
  } else {
    alert('Проверьте заполнение полей формы.');
  }
 
  inputs.forEach(elem => {
    elem.value = "";
  });
});

console.log('wizard');